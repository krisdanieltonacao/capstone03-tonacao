import {Button, Carousel} from "react-bootstrap";

export default function Banner(){
	return(
<Carousel>
  <Carousel.Item interval={1000}>
    <img
      className="d-block w-100"
      src="https://i.ibb.co/7zkHX7b/godofwar1920x712.jpg"
      alt="godofwar1920x712"
      border="0"
    />
    <Carousel.Caption>
      <Button class="btn btn-primary" href="#" role="button" size="sm">Order Now</Button>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item interval={1000}>
    <img
      className="d-block w-100"
      src="https://i.ibb.co/6rjLnS7/rockman1920-712.jpg"
      alt="rockman1920-712"
      border="0"
    />
    <Carousel.Caption>
      <Button class="btn btn-primary" href="#" role="button" size="sm">Order Now</Button>
    </Carousel.Caption>
  </Carousel.Item>  
  <Carousel.Item interval={1000}>
    <img
      className="d-block w-100"
      src="https://i.ibb.co/qgLXMT8/tekken7-1920x712.jpg"
      alt="tekken7-1920x712"
      border="0"
    />
    <Carousel.Caption>
      <Button class="btn btn-primary" href="#" role="button" size="sm">Order Now</Button>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item interval={1000}>
    <img
      className="d-block w-100"
      src="https://i.ibb.co/k2QKCpY/uncharted1920x712.jpg"
      alt="uncharted1920x712"
      border="0"
    />
    <Carousel.Caption>
      <Button class="btn btn-primary" href="#" role="button" size="sm">Order Now</Button>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item interval={1000}>
    <img
      className="d-block w-100"
      src="https://i.ibb.co/26c7J90/street1920x712.jpg"
      alt="street1920x712"
      border="0"
    />
    <Carousel.Caption>
      <Button class="btn btn-primary" href="#" role="button" size="sm">Order Now</Button>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item interval={1000}>
    <img
      className="d-block w-100"
      src="https://i.ibb.co/4SnJvJ4/nba2k22-1920x712.jpg"
      alt="nba2k22-1920x712"
      border="0"
    />
    <Carousel.Caption>
      <Button class="btn btn-primary" href="#" role="button" size="sm">Order Now</Button>
    </Carousel.Caption>
  </Carousel.Item>
</Carousel>
 
	)
}