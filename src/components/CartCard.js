import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Card, Col, Button, Row} from "react-bootstrap";
import 'animate.css';

export default function CartCard({orderProp}){
	const {_id, name, description01, description02, price, stocks,image005} = orderProp;
	return (
		<Card className="p-3 my-3" fluid>
		    <Row>
		    	<Col sm={4}>
		    	<img className="img-fluid animate__animated animate__pulse" src={image005} width="300" height="300" alt={name}/>
		    	</Col>
		    	<Col sm={7}>
		    <Card.Body fluid>
		        <Card.Title>
		            {name}
		        </Card.Title>
		        <Card.Subtitle>Description: </Card.Subtitle>
		        <Card.Text>
		            {description02}
		        </Card.Text>
		        <Card.Subtitle>Price: </Card.Subtitle>
		        <Card.Text>
		            {price}
		        </Card.Text>
		        <Card.Text>
		            Stocks: {stocks}
		        </Card.Text>
		        <Button as={Link} to={`/products/${_id}`} variant="primary">Details</Button>
		    </Card.Body>
		    </Col>
		    </Row>
		</Card>
	)
}

