import {Button, Card} from 'react-bootstrap';
import {Link} from "react-router-dom";

export default function HotSellingProducts(){
	return(
		<>
		<h4 className="text-center py-5">
			Other Featured Products
		</h4>
    <div className="d-md-flex justify-content-around">
		 <Card border="info" style={{ width: '28rem' }}>
      <Card.Img className="img-fluid animate__animated animate__pulse" variant="top" src="https://i.ibb.co/j6kbXJV/iphone13-Pro-Max-Graphite-005.jpg"/>
      <Card.Body>
        <Card.Title className="text-center">Iphone 13 Pro Max - Graphite</Card.Title>
        <Card.Text className="text-center">
        128 GB - Graphite
        </Card.Text>
        <Button as={Link} to={`/products/62e3fba186dfa82cfc13ca86`} variant="primary">Details</Button>
      </Card.Body>
    </Card>
    <Card border="info" style={{ width: '28rem' }}>
      <Card.Img className="img-fluid animate__animated animate__pulse" variant="top" src="https://i.ibb.co/SvMZNk1/Note20-Ultra-1400x700-005-black.jpg" />
      <Card.Body>
        <Card.Title className="text-center">Galaxy Note 22Ultra - 128GB - Black</Card.Title>
        <Card.Text className="text-center">
        128GB | 8GB | Black
        </Card.Text>
        <Button as={Link} to={`/products/62e90a7d5c0b255fe4118191`} variant="primary">Details</Button>
      </Card.Body>
    </Card>
    <Card border="info" style={{ width: '28rem' }}>
      <Card.Img className="img-fluid animate__animated animate__pulse" variant="top" src="https://i.ibb.co/qn6K4pT/ipadmini6grey-005.jpg" />
      <Card.Body>
        <Card.Title className="text-center">Ipad mini 6</Card.Title>
        <Card.Text className="text-center">
        64GB - GREY
        </Card.Text>
        <Button as={Link} to={`/products/62e50c124ceaa46a69e27c22`} variant="primary">Details</Button>
      </Card.Body>
    </Card>
    </div>
  
    </>
	)
}