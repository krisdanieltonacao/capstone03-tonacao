import {useState, useEffect} from "react";
import {Table, Button} from "react-bootstrap";

export default function AdShippedOrders(){
	const [allOrders, setAllOrders] = useState();
	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/allOrders`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setAllOrders(data.map(orderProp => {
				return(
						(orderProp.status === "Shipped")
						?
							<tr key={orderProp._id}>
                                <td>{orderProp.createdOn}</td>
								<td>{orderProp._id}</td>
                                <td>{orderProp.lastName+", "+orderProp.firstName}</td>
                                <td>{orderProp.mobileNo}</td>
                                <td>{orderProp.address}</td>
                                <td><img src={orderProp.image005} width="80" height="80" alt={orderProp.name}/></td>
								<td>{orderProp.name}</td>
								<td>{orderProp.price}</td>
                                <td>{orderProp.quantity}</td>
                                <td>{orderProp.subTotal}</td>
								<td>{orderProp.status}</td>
								<td>
								{
									<>
									<Button variant="primary" size="sm" onClick ={() => paid(orderProp._id, orderProp.name, orderProp._id, orderProp.lastName, orderProp.firstName)}>Paid</Button>
									<Button variant="success" size="sm" onClick ={() => complete(orderProp._id, orderProp.name, orderProp._id, orderProp.lastName, orderProp.firstName)}>Complete</Button>
									</>	
								}
								</td>
							</tr>
						:
							<tr></tr>
					)
			}))
		})
	}
	const paid = (productId, productName, productOrderId, productCustomerLastName, productCustomerFirstName) =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/status`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				status: "Paid"
			})
		})
		.then(res => res.json())
		.then(data =>{
			fetchData();
			window.location.reload(true);
		})
	}
	const complete = (productId, productName, productOrderId, productCustomerLastName, productCustomerFirstName) =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/status`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				status: "Complete"
			})
		})
		.then(res => res.json())
		.then(data =>{
			fetchData();
			window.location.reload(true);
		})
	}
	
	useEffect(()=>{
		fetchData();
	}, [])
	return(
		<>
			<Table striped bordered hover>
		     <thead>
		       <tr>
		         <th className="col-1">Date Ordered</th>
				 <th className="col-1">Order No.</th>
                 <th className="col-2">Name of Customer</th>
                 <th className="col-1">Mobile Number</th>
                 <th className="col-2">Address</th>
		         <th className="col-1">Image</th>
				 <th className="col-2">Name</th>
				 <th className="col-1">Price</th>
                 <th className="col-1">Qty</th>
                 <th className="col-1">Total</th>
				 <th className="col-1">Status</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allOrders }
		    	<tr>
					<td colspan="12" className="text-center">End of Transaction</td>
				</tr>
			 </tbody>
             </Table>
             </>		
)}