import { Link } from "react-router-dom";
import {useContext} from "react";
import UserContext from "../UserContext"
import {Navbar, Nav, Container} from "react-bootstrap";
import '../App.css';

export default function AppNavbar(){
	const {user} = useContext(UserContext);
	return(
	<>
		<Navbar className="bg-lblue navbar-dark">
      <Container fluid>
        <Navbar.Brand as={Link} to="/" >
          <img className="fluid" src="https://i.ibb.co/Nxdk789/logo-Gamingcon.png" style={{width: 200}} alt="GamingCon"></img>
        </Navbar.Brand>
        <Nav className="justify-content-end flex-grow-1 pe-3">
            {
              (user.id !== null)
                ?
                <>{
                  (user.isAdmin)
                  ?
                  <>
                      <Nav.Link className="text-light" href="/admin">Admin Dashboard</Nav.Link>
                      <Nav.Link className="text-light" href="/logout">Logout</Nav.Link>
                  </>
                  :
                   <>
                      
                      <Nav.Link className="text-light" href="/cart">Cart</Nav.Link>
                      <Nav.Link className="text-light" href="/logout">Logout</Nav.Link>
                    </>
                }</>
                :
                <>
                <Nav.Link className="text-light" href="/login">Login</Nav.Link>
                <Nav.Link className="text-light" href="/register">Register</Nav.Link>
                </>
            }
        </Nav>
        </Container>
      </Navbar>
		<Navbar>
      
    </Navbar>
    <Navbar className="bg-lblue" expand="lg m">
      <Container fluid>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="justify-content-center flex-grow-1 pe-5"
            style={{ maxHeight: '250px' }}
            navbarScroll
          >
			     <Nav.Link className="text-light" href="/">Home</Nav.Link>
            <Nav.Link className="text-light" href="/all/NSwitch">NSwitch</Nav.Link>
            <Nav.Link className="text-light" href="/all/Xbox">Xbox</Nav.Link>
            <Nav.Link className="text-light" href="/all/PS4">PS4</Nav.Link>
            <Nav.Link className="text-light" href="/all/PS5">PS5</Nav.Link>
            <Nav.Link className="text-light" href="/all/APPLE">Apple</Nav.Link>
            <Nav.Link className="text-light" href="/all/Samsung">Samsung</Nav.Link>
        </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
    </>
	)}
