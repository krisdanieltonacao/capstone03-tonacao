import React from 'react';
import {Button, Card} from "react-bootstrap";
import {Link} from "react-router-dom";

export default function HotTitles(){
	return(
<>
		<h4 className="text-center pt-5 pb-2">
			Game Consoles
		</h4>
<div className="d-md-flex justify-content-center">
<Card className="mx-2" border="info" style={{ width: '18rem' }}>
        <Card.Header>Nintendo Switch</Card.Header>
        <Card.Body>
          <Card.Title><img className="img-fluid animate__animated animate__pulse" src="https://i.ibb.co/LRnQG9L/NSwitch1400x700-005.jpg" width="180" height="180" alt="Nintendo Switch"/></Card.Title>
          <Card.Text>
		  Nintendo Switch OLED [White]
          </Card.Text>
		  <Button as={Link} to={`/products/62e901a55c0b255fe4118174`} variant="primary">Details</Button>
        </Card.Body>
      </Card>
	  <Card className="mx-2" border="info" style={{ width: '18rem' }}>
        <Card.Header>Xbox</Card.Header>
        <Card.Body>
          <Card.Title><img className="img-fluid animate__animated animate__pulse" src="https://i.ibb.co/cY5WwCK/Xbox350x350-005.jpg" width="180" height="180" alt="Xbox"/></Card.Title>
          <Card.Text>
		  Xbox One [White]
          </Card.Text>
		  <Button as={Link} to={`/products/62e901d75c0b255fe4118176`} variant="primary">Details</Button>
        </Card.Body>
      </Card>
	  <Card className="mx-2" border="info" style={{ width: '18rem' }}>
        <Card.Header>PlayStation 5</Card.Header>
        <Card.Body>
          <Card.Title><img className="img-fluid animate__animated animate__pulse" src="https://i.ibb.co/0BMvFgZ/ps5-350x350-005.jpg" width="180" height="180" alt="PlayStation 5"/></Card.Title>
          <Card.Text>
		  PlayStation 5 [Disc Version]
          </Card.Text>
		  <Button as={Link} to={`/products/62e902105c0b255fe4118178`} variant="primary">Details</Button>
        </Card.Body>
      </Card>
	  <Card className="mx-2" border="info" style={{ width: '18rem' }}>
        <Card.Header>PlayStation 4</Card.Header>
        <Card.Body>
          <Card.Title><img className="img-fluid animate__animated animate__pulse" src="https://i.ibb.co/wCR8325/ps4-350x350-005.jpg" width="180" height="180" alt="PlayStation 4"/></Card.Title>
          <Card.Text>
		  PlayStation 4 Pro
          </Card.Text>
		  <Button as={Link} to={`/products/62e902465c0b255fe411817a`} variant="primary">Details</Button>
        </Card.Body>
      </Card>
	  </div>

</>
)
}


  