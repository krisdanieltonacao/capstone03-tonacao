import {useState, useEffect } from "react";
import {Table} from "react-bootstrap";

export default function ShippedOrders(){

	const [allOrders, setAllOrders] = useState();


	const fetchData = () =>{
		
		fetch(`${process.env.REACT_APP_API_URL}/users/cart`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setAllOrders(data.map(orderProp => {
				
				return(
						(orderProp.status === "Shipped")
						?
						<tr key={orderProp._id}>
						<td>{orderProp.createdOn}</td>
						<td><img src={orderProp.image005} width="100" height="100" alt={orderProp.name}/></td>
						<td>{orderProp.name}</td>
						<td>{orderProp.price}</td>
						<td>{orderProp.quantity}</td>
						<td>{orderProp.subTotal}</td>
						<td>{orderProp.status}</td>
						</tr>
						:
						
					<tr></tr>	
				
					)
			}))

		})
	}
	
	

	useEffect(()=>{
		fetchData();
	}, [])

	return(
		<>
		    <div className="mt-5 mb-3">
				<h6>Please check all your Shipped Orders below:</h6>
				    </div>
			<Table striped bordered hover>
		     <thead>
		       <tr>
			   <th className="col-1">Date Ordered</th>
		         <th className="col-1">Image</th>
				 <th className="col-5">Name</th>
				 <th className="col-1">Price</th>
				 <th className="col-1">Qty</th>
				 <th className="col-1">SubTotal</th>
				 <th className="col-1">Status</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allOrders }
               <tr>
					<td colspan="12" className="text-center">End of Transaction</td>
					
				</tr>
			 </tbody>
             </Table>
             
             </>		
)
    }