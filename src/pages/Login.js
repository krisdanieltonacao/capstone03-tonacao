import {useState, useEffect, useContext} from "react";
import { Navigate } from "react-router-dom";
import {Button, Col, Form} from "react-bootstrap";
import UserContext from "../UserContext";

import Swal from "sweetalert2";


export default function Login(){
	const {user, setUser} = useContext(UserContext);
	const [email, setEmail] = useState("");
	const [password01, setPassword01] = useState("");
	const [isActive, setIsActive] = useState(false);

	function login(e){
		e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({

				email: email,
				password01: password01
			})
		})
		.then(res => res.json())
		.then(data =>{
			if(typeof data.access !== "undefined"){
				localStorage.setItem("token", data.access);
				retrieveUserDetails(data.access);
				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Happy Shopping"
				});
			}
			else{
				Swal.fire({
					title: "Authetication Failed",
					icon: "error",
					text: "Check your login details and try again."
				});
			}
		})
		setEmail("");
		setPassword01("");
	}

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() =>{
	
		if(email !== "" && password01 !== ""){
			setIsActive(true)
		}
		else{
			setIsActive(false)
		}
	}, [email, password01])

	return(
		(user.id !== null)
		?	
			<Navigate to="/"/>
		:
		<>
		<h1 className="my-5 text-center">Login</h1>
		<Form onSubmit ={(e) => login(e)}>
		<div className="d-md-flex">
			<Col className="px-5"></Col>
			<Col className="px-5">
			<Form.Group className="mb-3" controlId="userEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
			</Form.Group>
			<Form.Group className="mb-3" controlId="password01">
				<Form.Label>Password</Form.Label>
				<Form.Control type="password" placeholder="Password" value={password01} onChange={e => setPassword01(e.target.value)}/>
			</Form.Group>
			</Col>
			<Col className="px-5">
			</Col>    
			</div>
			<div className="d-flex justify-content-center py-2">
			{
					isActive
					?
					<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
					:
						<Button variant="primary" type="submit" id="submitBtn" disabled>
						  Submit
						</Button>
				}
			</div>
		</Form>
		</>
	)
}
