
import CompletedOrders from "../components/CompletedOrders";
import PendingOrders from "../components/PendingOrders";
import PaidOrders from "../components/PaidOrders";
import ShippedOrders from "../components/ShippedOrders";
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';



export default function CartPage() {
  return (
	<>
	<h3 className="text-center py-3">My Cart</h3>
	<h6 className="text-center pb-3">Click on a tab to check desired status</h6>
    <Tabs
      defaultActiveKey="Pending"
      id="justify-tab-example"
      className="mb-3"
      justify
    >
      <Tab eventKey="Pending" title="PENDING">
	  <PendingOrders />
      </Tab>
      <Tab eventKey="Paid" title="PAID">
	  <PaidOrders />
      </Tab>
      <Tab eventKey="Shipped" title="SHIPPED">
	  <ShippedOrders />
      </Tab>
      <Tab eventKey="Completed" title="COMPLETE">
	  <CompletedOrders />
      </Tab>
    </Tabs>
	</>
  );
}

