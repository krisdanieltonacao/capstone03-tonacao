import {useEffect, useState, useContext} from "react";
import {Navigate} from "react-router-dom";
import UserContext from "../UserContext";

import ProductCard from "../components/ProductCard";

export default function Apple(){

const [products, setProducts] = useState([]);
const {user} = useContext(UserContext);
	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/all/APPLE`)
		.then(res => res.json())
		.then(data => {
			setProducts(data.map(product =>{
				return(
					<ProductCard key={product._id} productProp={product} />
				)
			}))
		})
	},[])


	return(
		(user.isAdmin)
		?
			<Navigate to="/admin" />
		:
		<>
			<h2 className="text-center py-2">Apple Products</h2>
			{products}
		</>
	)
}