import {useState, useEffect, useContext} from "react";
import {Button, Col, Form} from "react-bootstrap";
import {useNavigate} from "react-router-dom";
import UserContext from "../UserContext";

import Swal from "sweetalert2";

export default function EditUserDetails(){
	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	const [email, setEmail] = useState("");
    const [password01, setPassword01] = useState("");
    const [password02, setPassword02] = useState("");
    const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	
	const [birthDate, setBirthDate] = useState("");
	const [address, setAddress] = useState("");
	const [company, setCompany] = useState("");
	const [accountNo, setAccountNo] = useState("");
	const [issuer, setIssuer] = useState("");
	const [cardNo, setCardNo] = useState("");
	const [expDate, setExpDate] = useState("");
	const [cvv, setCvv] = useState("");

	function registerUser(e){
		e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/edit`, {
					method: "PUT",
					headers:{
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password01: password01,
						birthDate: birthDate,
						address: address,
						company: company,
						accountNo: accountNo,
						issuer: issuer,
						cardNo: cardNo,
						expDate: expDate,
						cvv: cvv
					})
				})
				.then(res => res.json())
				.then(data => {
					if(data){
						setFirstName("");
						setLastName("");
						setMobileNo("");
						setEmail("");
						setPassword01("");
						setPassword02("");
						setBirthDate("");
						setAddress("");
						setCompany("");
						setAccountNo("");
						setIssuer("");
						setCardNo("");
						setExpDate("");
						setCvv("");


						Swal.fire({
							title: "Details Successfully Updated",
							icon: "success",
							text: "SAVED"
						})
						navigate("/");
					}
					else{
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			}
	
	useEffect(()=>{
	   	fetch(`${process.env.REACT_APP_API_URL}/users/edit`)
    	.then(res => res.json())
    	.then(data => {
    		setEmail(data.email);
	    	setPassword01(data.password01);
            setPassword02(data.password01);
            setFirstName(data.firstName);
            setLastName(data.lastName);
            setMobileNo(data.mobileNo);
            setBirthDate(data.birthDate);
            setAddress(data.address);
            setCompany(data.company);
            setAccountNo(data.accountNo);
            setIssuer(data.issuer);
            setCardNo(data.cardNo);
            setExpDate(data.expDate);
            setCvv(data.cvv);
    	});

    }, []);

	return(
		(user._id !== null)
		?
			<tr></tr>
		:
		<>
			<h1 className="my-5 text-center">Register New User Account</h1>		
			<Form onSubmit = {(e) => registerUser(e)}>
			<div className="d-md-flex">
			<Col className="px-5">
				<Form.Group className="mb-3" controlId="userEmail">
			    	<Form.Label>Email address</Form.Label>
			        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
			    </Form.Group>
			    <Form.Group className="mb-3" controlId="password01">
			    	<Form.Label>Password</Form.Label>
			    	<Form.Control type="password" placeholder="Password"  value={password01} onChange={e => setPassword01(e.target.value)}/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="password02">
					<Form.Label>Verify Password</Form.Label>
					<Form.Control type="password" placeholder="Verify Password"  value={password02} onChange={e => setPassword02(e.target.value)}/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="firstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control type="text" placeholder="First Name" value={firstName} onChange={e => setFirstName(e.target.value)}/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="lastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control type="text" placeholder="Last Name" value={lastName} onChange={e => setLastName(e.target.value)}/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="mobileNo">
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control type="text" placeholder="Mobile Number" value={mobileNo} onChange={e => setMobileNo(e.target.value)}/>
				</Form.Group>
			</Col>

			<Col className="px-5">
				<Form.Group className="mb-3">
			    	<Form.Label>Birthdate</Form.Label>
			        <Form.Control type="date" placeholder="Birthdate" value={birthDate} onChange={e => setBirthDate(e.target.value)}/>
			    </Form.Group>
			    <Form.Group className="mb-3">
			    	<Form.Label>Billing Address:</Form.Label>
			    	<div  className="d-md-flex">
			    		<Form.Control type="text" placeholder="Address" value={address} onChange={e => setAddress(e.target.value)}/>
			    	</div>
				</Form.Group>
				<Form.Label>Payment Information:</Form.Label>
				<Form.Group className="mb-3">
					<div className="d-md-flex">
					<div>
					<Form.Label>E-Wallet:</Form.Label>
						<Form.Control type="text" placeholder="E-Wallet Company" value={company} onChange={e => setCompany(e.target.value)}/>
					</div>
					<div>
					<Form.Label>Account No:</Form.Label>
						<Form.Control type="text" placeholder="Account Number" value={accountNo} onChange={e => setAccountNo(e.target.value)}/>
					</div>
					</div>
				</Form.Group>
				<Form.Group className="mb-3">
					<div className="d-md-flex">
					<div>
					<Form.Label>Card Payment:</Form.Label>
						<Form.Control type="text" placeholder="Bank Issuer" value={issuer} onChange={e => setIssuer(e.target.value)}/>
					</div>
					<div>
					<Form.Label>Card No:</Form.Label>
						<Form.Control type="text" placeholder="Card Number" value={cardNo} onChange={e => setCardNo(e.target.value)}/>
						<Form.Control type="text" placeholder="Expiry Date(mm/yyyy)" value={expDate} onChange={e => setExpDate(e.target.value)}/>
						<Form.Control type="text" placeholder="CVV" value={cvv} onChange={e => setCvv(e.target.value)}/>

					</div>
					</div>
				</Form.Group>
				
			</Col>    
			</div>
			<div className="d-flex justify-content-center py-2">
			<Form.Group className="px-3">
  			  		<Button className="btn-lg" variant="primary" type="submit" id="submitBtn">Submit</Button>
		    </Form.Group>
			<Form.Group className="px-3">
				<Button className="btn-lg" variant="danger" type="submit" id="submitBtn">Reset</Button>
			</Form.Group>
			</div>
			</Form>
			
			
		</>
	)
}