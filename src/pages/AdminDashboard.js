import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";


import Swal from "sweetalert2";

export default function AdminDashboard(){

	const {user} = useContext(UserContext);
	const [allProducts, setAllProducts] = useState([]);

	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/all`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
			setAllProducts(data.map(product => {
				
				return(
						<tr key={product._id}>
						<td>{product.type}</td>
						<td><img src={product.image005} width="80" height="80" alt={product.name}/></td>
						<td>{product.name}</td>
						<td>{product.description01}</td>
						<td>{product.description02}</td>
						<td>{product.price}</td>
						<td>{product.stocks}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								// We use conditional rendering to set which button should be visible based on the course status (active/inactive)
								(product.isActive)
								?	
								 	// A button to change the course status to "Inactive"
									<Button variant="danger" size="sm" onClick ={() => archive(product._id, product.name)}>Archive</Button>
								:
									<>
										{/* A button to change the course status to "Active"*/}
										<Button variant="success" size="sm" onClick ={() => unarchive(product._id, product.name)}>Unarchive</Button>
										{/* A button to edit a specific course*/}
										<Button as={ Link } to={`/editProduct/${product._id}`} variant="secondary" size="sm" className="m-2" >Edit</Button>
									</>
							}
						</td>
					</tr>
				)
			}))

		})
	}

	//Making the course inactive
	const archive = (productId, productName) =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			if(data){
				Swal.fire({
					title: "Archive Succesful!",
					icon: "success",
					text: `${productName} is now inactive.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	const unarchive = (productId, productName) =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			if(data){
				Swal.fire({
					title: "Unarchive Succesful!",
					icon: "success",
					text: `${productName} is now active.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	useEffect(()=>{
		fetchData();
	}, [])

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				<Button as={Link} to="/addProduct" variant="primary" size="lg" className="mx-2">Add Product</Button>
				<Button as={Link} to="/admin" variant="grey" size="lg" className="mx-2" disabled>Show Products</Button>
				<Button as={Link} to="/allOrders" variant="danger" size="lg" className="mx-2">Show Orders</Button>
				<Button as={Link} to="/archived" variant="secondary" size="lg" className="mx-2">Archived Orders</Button>
			</div>
			
			<Table striped bordered hover>
		     <thead>
		       <tr>
		         <th>Type</th>
		         <th>Image</th>
		         <th>Game Title</th>
		         <th>Console</th>
		         <th>Description</th>
		         <th>Price</th>
		         <th>Slots</th>
		         <th>Status</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allProducts }
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/products" />
	)
}