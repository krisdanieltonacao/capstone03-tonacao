import {useState, useEffect, useContext} from "react";
import {Button, Col, Form} from "react-bootstrap";
import {Navigate, useNavigate} from "react-router-dom";
import UserContext from "../UserContext";

import Swal from "sweetalert2";

export default function Register(){
	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	const [email, setEmail] = useState("");
	const [password01, setPassword01] = useState("");
	const [password02, setPassword02] = useState("");
	
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	
	const [birthDate, setBirthDate] = useState("");
	const [houseNo, setHouseNo] = useState("");
	const [street, setStreet] = useState("");
	const [barangay, setBarangay] = useState("");
	const [city, setCity] = useState("");
	const [province, setProvince] = useState("");
	const [postalCode, setPostalCode] = useState("");
	const [company, setCompany] = useState("");
	const [accountNo, setAccountNo] = useState("");
	const [issuer, setIssuer] = useState("");
	const [cardNo, setCardNo] = useState("");
	const [expDate, setExpDate] = useState("");
	const [cvv, setCvv] = useState("");

	function registerUser(e){
		e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers:{
				"Content-Type":"application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Kindly provide another email to complete the registration."
				})
			}
			else{
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers:{
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password01: password01,
						birthDate: birthDate,
						address: houseNo+" "+street+" "+barangay+", "+city+", "+province+". "+postalCode,
						company: company,
						accountNo: accountNo,
						issuer: issuer,
						cardNo: cardNo,
						expDate: expDate,
						cvv: cvv
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data){
						//Clear input fields
						setFirstName("");
						setLastName("");
						setMobileNo("");
						setEmail("");
						setPassword01("");
						setPassword02("");
						setBirthDate("");
						setHouseNo("");
						setStreet("");
						setBarangay("");
						setCity("");
						setProvince("");
						setPostalCode("");
						setCompany("");
						setAccountNo("");
						setIssuer("");
						setCardNo("");
						setExpDate("");
						setCvv("");


						Swal.fire({
							title: "Registration successful",
							icon: "success",
							text: "Happy Shopping!"
						})

						//redirect the user to the login page after registration.
						navigate("/login");
					}
					else{
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			}
		})
	}
	const [isActive, setIsActive] = useState(false);

	useEffect(()=>{
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo.length === 11 && password01 !== "" && password02 !== "") && (password01 === password02))
			{
				setIsActive(true);
			}
		else
			{
				setIsActive(false);
			}
	},[firstName, lastName, email, mobileNo, password01, password02, birthDate, houseNo, street, barangay, city, province, postalCode, company, accountNo, issuer, cardNo, expDate, cvv])

	return(
		(user.id !== null)
		?
			<Navigate to="/" />
		:
		<>
			<h1 className="my-5 text-center">Register New User Account</h1>		
			<Form onSubmit = {(e) => registerUser(e)}>
			<div className="d-md-flex">
			<Col className="px-5">
				<Form.Group className="mb-3" controlId="userEmail">
			    	<Form.Label>Email address</Form.Label>
			        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
			    </Form.Group>
			    <Form.Group className="mb-3" controlId="password01">
			    	<Form.Label>Password</Form.Label>
			    	<Form.Control type="password" placeholder="Password"  value={password01} onChange={e => setPassword01(e.target.value)}/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="password02">
					<Form.Label>Verify Password</Form.Label>
					<Form.Control type="password" placeholder="Verify Password"  value={password02} onChange={e => setPassword02(e.target.value)}/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="firstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control type="text" placeholder="First Name" value={firstName} onChange={e => setFirstName(e.target.value)}/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="lastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control type="text" placeholder="Last Name" value={lastName} onChange={e => setLastName(e.target.value)}/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="mobileNo">
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control type="text" placeholder="Mobile Number" value={mobileNo} onChange={e => setMobileNo(e.target.value)}/>
				</Form.Group>
			</Col>

			<Col className="px-5">
				<Form.Group className="mb-3">
			    	<Form.Label>Birthdate</Form.Label>
			        <Form.Control type="date" placeholder="Birthdate" value={birthDate} onChange={e => setBirthDate(e.target.value)}/>
			    </Form.Group>
			    <Form.Group className="mb-3">
			    	<Form.Label>Billing Address:</Form.Label>
			    	<div  className="d-md-flex">
			    		<Form.Control type="text" placeholder="House No." value={houseNo} onChange={e => setHouseNo(e.target.value)}/>
			    		<Form.Control type="text" placeholder="Street" value={street} onChange={e => setStreet(e.target.value)}/>
			    		<Form.Control type="text" placeholder="Barangay" value={barangay} onChange={e => setBarangay(e.target.value)}/>
			    	</div>
			    	<div  className="d-md-flex py-2">
			    		<Form.Control type="text" placeholder="City" value={city} onChange={e => setCity(e.target.value)}/>
			    		<Form.Control type="text" placeholder="Province" value={province} onChange={e => setProvince(e.target.value)}/>
			    		<Form.Control type="text" placeholder="Postal Code" value={postalCode} onChange={e => setPostalCode(e.target.value)}/>
			    	</div>
				</Form.Group>
				<Form.Label>Payment Information:</Form.Label>
				<Form.Group className="mb-3">
					<div className="d-md-flex">
					<div>
					<Form.Label>E-Wallet:</Form.Label>
						<Form.Control type="text" placeholder="E-Wallet Company" value={company} onChange={e => setCompany(e.target.value)}/>
					</div>
					<div>
					<Form.Label>Account No:</Form.Label>
						<Form.Control type="text" placeholder="Account Number" value={accountNo} onChange={e => setAccountNo(e.target.value)}/>
					</div>
					</div>
				</Form.Group>
				<Form.Group className="mb-3">
					<div className="d-md-flex">
					<div>
					<Form.Label>Card Payment:</Form.Label>
						<Form.Control type="text" placeholder="Bank Issuer" value={issuer} onChange={e => setIssuer(e.target.value)}/>
					</div>
					<div>
					<Form.Label>Card No:</Form.Label>
						<Form.Control type="text" placeholder="Card Number" value={cardNo} onChange={e => setCardNo(e.target.value)}/>
						<Form.Control type="text" placeholder="Expiry Date(mm/yyyy)" value={expDate} onChange={e => setExpDate(e.target.value)}/>
						<Form.Control type="text" placeholder="CVV" value={cvv} onChange={e => setCvv(e.target.value)}/>

					</div>
					</div>
				</Form.Group>
				
			</Col>    
			</div>
			<div className="d-flex justify-content-center py-2">
			<Form.Group className="px-3">
  			 {
		      	isActive
		      	?
		      		<Button className="btn-lg" variant="primary" type="submit" id="submitBtn">Submit</Button>
		      	:
		      		<Button className="btn-lg" variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
		      }
			</Form.Group>
			<Form.Group className="px-3">
				<Button className="btn-lg" variant="danger" type="submit" id="submitBtn">Reset</Button>
			</Form.Group>
			</div>
			</Form>
			
			
		</>
	)
}