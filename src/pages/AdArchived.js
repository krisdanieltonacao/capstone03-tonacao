import {useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {Link} from "react-router-dom";

export default function AdArchived(){
	const [allOrders, setAllOrders] = useState();
	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/allOrders`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setAllOrders(data.map(orderProp => {
				
				return(
					
					
						(orderProp.status === "Archived")
						?
							<tr key={orderProp._id}>
                                <td>{orderProp.createdOn}</td>
								<td>{orderProp._id}</td>
                                <td>{orderProp.lastName+", "+orderProp.firstName}</td>
                                <td>{orderProp.mobileNo}</td>
                                <td>{orderProp.address}</td>
                                <td><img src={orderProp.image005} width="80" height="80" alt={orderProp.name}/></td>
								<td>{orderProp.name}</td>
								<td>{orderProp.price}</td>
                                <td>{orderProp.quantity}</td>
                                <td>{orderProp.subTotal}</td>
								<td>{orderProp.status}</td>
								<td>
								{
									<>
										<Button variant="danger" size="sm" onClick ={() => pending(orderProp._id, orderProp.name, orderProp._id, orderProp.lastName, orderProp.firstName)}>Pending</Button>
										<Button variant="primary" size="sm" onClick ={() => paid(orderProp._id, orderProp.name, orderProp._id, orderProp.lastName, orderProp.firstName)}>Paid</Button>
									    <Button variant="warning" size="sm" onClick ={() => shipped(orderProp._id, orderProp.name, orderProp._id, orderProp.lastName, orderProp.firstName)}>Shipped</Button>
									    <Button variant="success" size="sm" onClick ={() => complete(orderProp._id, orderProp.name, orderProp._id, orderProp.lastName, orderProp.firstName)}>Complete</Button>
									</>	
								}			
								</td>
							</tr>
						:
							<tr></tr>
					)
			}))

		})
	}
	const paid = (productId, productName, productOrderId, productCustomerLastName, productCustomerFirstName) =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/status`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				status: "Paid"
			})
		})
		.then(res => res.json())
		.then(data =>{
			fetchData();
			window.location.reload(true);
			
		})
	}

	const pending = (productId, productName, productOrderId, productCustomerLastName, productCustomerFirstName) =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/status`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				status: "Pending"
			})
		})
		.then(res => res.json())
		.then(data =>{
			fetchData();
			window.location.reload(true);
			
		})
	}
	
	const shipped = (productId, productName, productOrderId, productCustomerLastName, productCustomerFirstName) =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/status`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				status: "Shipped"
			})
		})
		.then(res => res.json())
		.then(data =>{
			fetchData();
			window.location.reload(true);
			
		})
	}
	
	const complete = (productId, productName, productOrderId, productCustomerLastName, productCustomerFirstName) =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/status`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				status: "Complete"
			})
		})
		.then(res => res.json())
		.then(data =>{
			fetchData();
			window.location.reload(true);
		})
	}
	
	useEffect(()=>{
		fetchData();
	}, [])


	return(
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				<Button as={Link} to="/addProduct" variant="primary" size="lg" className="mx-2">Add Product</Button>
				<Button as={Link} to="/admin" variant="success" size="lg" className="mx-2" disabled>Show Products</Button>
				<Button as={Link} to="/allOrders" variant="danger" size="lg" className="mx-2">Show Orders</Button>
				<Button as={Link} to="/archived" variant="grey" size="lg" className="mx-2">Archived Orders</Button>
			</div>
			
			<h4 className="py-4 text-center">Archived Orders</h4>
			<Table striped bordered hover>
		     <thead>
		       <tr>
		         <th className="col-1">Date Ordered</th>
				 <th className="col-1">Order No.</th>
                 <th className="col-2">Name of Customer</th>
                 <th className="col-1">Mobile Number</th>
                 <th className="col-2">Address</th>
		         <th className="col-1">Image</th>
				 <th className="col-2">Name</th>
				 <th className="col-1">Price</th>
                 <th>Qty</th>
                 <th className="col-1">Total</th>
				 <th>Status</th>
				 <th className="col-1">Restore to</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allOrders }
		    	<tr>
					<td colspan="12" className="text-center">End of Transaction</td>
					
				</tr>
			 </tbody>
             </Table>
             
             </>		
)
    }