import { useState, useEffect, useContext } from 'react';
import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

import { Form, Button } from 'react-bootstrap';

export default function EditProduct() {

	const {user} = useContext(UserContext);
	const { productId } = useParams();

	const navigate = useNavigate();


	const [type, setType] = useState('');
	const [name, setName] = useState('');
	const [description01, setDescription01] = useState('');
	const [description02, setDescription02] = useState('');
	const [stocks, setStocks] = useState(0);
    const [price, setPrice] = useState(0);
    const [imageMain, setImageMain] = useState('');
    const [video, setVideo] = useState('');
    const [image002, setImage002] = useState('');
    const [image003, setImage003] = useState('');
    const [image004, setImage004] = useState('');
    const [image005, setImage005] = useState('');

    const [isActive, setIsActive] = useState(false);


	function editProduct(e) {
	    e.preventDefault();

	    fetch(`http://localhost:4000/products/${productId}`, {
	    	method: "PUT",
	    	headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
			    type: type,
			    name: name,
			    description01: description01,
			    description02: description02,
			    stocks: stocks,
				price: price,
			    imageMain: imageMain,
			    video: video,
			    image002: image002,
			    image003: image003,
			    image004: image004,
			    image005: image005
			})
	    })
	    .then(res => res.json())
	    .then(data => {

	    	if(data){
	    		Swal.fire({
	    		    title: "Product Succesfully Updated",
	    		    icon: "success",
	    		    text: `${name} is now updated`
	    		});

	    		navigate("/admin");
	    	}
	    	else{
	    		Swal.fire({
	    		    title: "Error!",
	    		    icon: "error",
	    		    text: `Something went wrong. Please try again later!`
	    		});
	    	}

	    })

	    // Clear input fields
	    setType('');
	    setName('');
	    setDescription01('');
	    setDescription02('');
	    setStocks(0);
	    setPrice(0);
	    setImageMain('');
	    setVideo('');
	    setImage002('');
	    setImage003('');
	    setImage004('');
	    setImage005('');

	}

	useEffect(() => {

        // Validation to enable submit button when all fields are populated and set a price and slot greater than zero.
         if(type!=="" && name !== "" && description01 && description02 !== "" && price > 0 && stocks > 0 && imageMain!=="" && video!=="" && image002!=="" && image003!=="" && image004!=="" && image005!==""){
            setIsActive(true);
        } else {
            setIsActive(false);
        }


   }, [type, name, description01, description02, price, stocks, imageMain, video, image002, image003, image004, image005]);

	//To get the information of the course to be updated.
    useEffect(()=> {
    
    	fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
    	.then(res => res.json())
    	.then(data => {
    		// Changing the initial state of the following to the information of the course to be edited.
    		setType(data.type);
	    	setName(data.name);
	    	setDescription01(data.description01);
	    	setDescription02(data.description02);
	    	setStocks(data.stocks);
	    	setPrice(data.price);
	    	setImageMain(data.imageMain);
	    	setVideo(data.video);
	    	setImage002(data.image002);
	    	setImage003(data.image003);
	    	setImage004(data.image004);
	    	setImage005(data.image005);
    	});

    }, [productId]);

    return (
    	user.isAdmin
    	?
			<>
		    	<h1 className="my-5 text-center">Edit Product</h1>
		        <Form onSubmit={(e) => editProduct(e)}>
		        	<Form.Group controlId="name" className="mb-3">
		                <Form.Label>Enter Type of Product</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Game / Console / Phone / Accessories" 
			                value = {type}
			                onChange={e => setType(e.target.value)}
			                required
		                />
		            </Form.Group>

		        	<Form.Group controlId="name" className="mb-3">
		                <Form.Label>Product Name</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter Course Name" 
			                value = {name}
			                onChange={e => setName(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="description01" className="mb-3">
		                <Form.Label>Console or Brand</Form.Label>
		                <Form.Control
		                	type="text"
		                	rows={3}
			                placeholder="Console/Brand" 
			                value = {description01}
			                onChange={e => setDescription01(e.target.value)}
			                required
		                />
		            </Form.Group>

					<Form.Group controlId="description02" className="mb-3">
		                <Form.Label>Description</Form.Label>
		                <Form.Control
		                	as="textarea"
		                	rows={3}
			                placeholder="Short Description" 
			                value = {description02}
			                onChange={e => setDescription02(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="slots" className="mb-3">
		                <Form.Label>Number of Stocks</Form.Label>
		                <Form.Control 
			                type="number" 
			                placeholder="Enter onHand Stocks" 
			                value = {stocks}
			                onChange={e => setStocks(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="price" className="mb-3">
		                <Form.Label>Unit Price</Form.Label>
		                <Form.Control 
			                type="number" 
			                placeholder="Enter Price" 
			                value = {price}
			                onChange={e => setPrice(e.target.value)}
			                required
		                />
		            </Form.Group>

					<Form.Group controlId="imageMain" className="mb-3">
		                <Form.Label>Main Image</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter Main Image URL" 
			                value = {imageMain}
			                onChange={e => setImageMain(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="video" className="mb-3">
		                <Form.Label>Video</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter Product Video URL" 
			                value = {video}
			                onChange={e => setVideo(e.target.value)}
			                required
		                />
		            </Form.Group>
		            
		            <Form.Group controlId="image002" className="mb-3">
		                <Form.Label>Image 002</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter Image 002 URL" 
			                value = {image002}
			                onChange={e => setImage002(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="image003" className="mb-3">
		                <Form.Label>Image 003</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter Image 002 URL" 
			                value = {image003}
			                onChange={e => setImage003(e.target.value)}
			                required
		                />
		            </Form.Group>
		            <Form.Group controlId="image004" className="mb-3">
		                <Form.Label>Image 004</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter Image 002 URL" 
			                value = {image004}
			                onChange={e => setImage004(e.target.value)}
			                required
		                />
		            </Form.Group>
		            <Form.Group controlId="image005" className="mb-3">
		                <Form.Label>Image 005</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter Image 002 URL" 
			                value = {image005}
			                onChange={e => setImage005(e.target.value)}
			                required
		                />
		            </Form.Group>

		            {/* conditionally render submit button based on isActive state */}
	        	    { isActive 
	        	    	? 
	        	    	<Button variant="primary" type="submit" id="submitBtn">
	        	    		Update
	        	    	</Button>
	        	        : 
	        	        <Button variant="danger" type="submit" id="submitBtn" disabled>
	        	        	Update
	        	        </Button>
	        	    }
	        	    	<Button className="m-2" as={Link} to="/admin" variant="success" type="submit" id="submitBtn">
	        	    		Cancel
	        	    	</Button>
		        </Form>
	    	</>
    	:
    	    <Navigate to="/products" />
	    	
    )

}