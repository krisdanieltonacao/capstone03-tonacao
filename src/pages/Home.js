import Banner from "../components/Banner";
import HotTitles from "../components/HotTitles";
import HotSellingProducts from "../components/HotSellingProducts";

export default function Home(){
	return(
		<>
			<Banner />
			<HotTitles />
			<HotSellingProducts />
    	</>
	)
}