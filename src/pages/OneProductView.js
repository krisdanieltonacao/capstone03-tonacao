import { useState, useEffect, useContext } from "react";
import { Link, useParams, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import UserContext from "../UserContext";
import ReactPlayer from "react-player";


export default function OneProductView(){
	const { user } = useContext(UserContext);
	const navigate = useNavigate();
	const {productId} = useParams();

	const [name, setName] = useState("");
	const [description02, setDescription02] = useState("");
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState(0);
	const [imageMain, setImageMain] = useState("");
	const [video, setVideo] = useState("");
	const [image002, setImage002] = useState("");
	const [image003, setImage003] = useState("");
	const [image004, setImage004] = useState("");
	const [image005, setImage005] = useState("");

	const purchase = (productId, name, image005, price) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/purchase/${productId}`,{
			method: "POST",
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId,
				name: name,
				image005: image005,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				Swal.fire({
					title: "Thank You!",
					icon: "success",
					text: "You have successfully purchased an item."
				})
				navigate("/");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	useEffect(() =>{
	
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name);
			setDescription02(data.description02);
			setPrice(data.price);
			setStocks(data.stocks);
			setImageMain(data.imageMain);
			setVideo(data.video);
			setImage002(data.image002);
			setImage003(data.image003);
			setImage004(data.image004);
			setImage005(data.image005);
		})

	}, [productId])

	return(
		<Container className="mt-3">
			<Row>
				<Col>
					<Card>
						<Card.Body>
						<Card.Title className="text-center py-3">{name}</Card.Title>
						   						<div className="d-md-flex justify-content-around py-2">
	      							<div className="py-2 col-lg-4 col-md-4 d-flex justify-content-sm-center">
	      								<img className="img-fluid animate__animated animate__pulse" src={image005} alt={name}/>
			    					</div>
									<div className="px-2">
										<Card.Subtitle className="py-2">Description:</Card.Subtitle>
										<Card.Text>{description02}</Card.Text>
										<Card.Subtitle className="py-2">Stocks Left:</Card.Subtitle>
										<Card.Text>{stocks}</Card.Text>
										<Card.Subtitle className="py-2">Price:</Card.Subtitle>
										<Card.Text>PhP {price}</Card.Text>
										<div className="d-grid gap-2 py-3">
										{
											(stocks >= 1)
											?
												
													(user.id !== null)
													?
													<Col>
														<Button variant="primary" size="lg" onClick={() => purchase(productId, name, image005, price)}>Buy Now</Button>
													</Col>
													:
													<Col>
														<h6>Please Log-In Account to Buy.</h6>
														<Button as={Link} to="/login" variant="primary" size="lg">Log-In</Button>
													</Col>
												
											:	
											<Col>
												<Button variant="danger" size="lg">Out of Stock</Button>
											</Col>

										}	
										</div>
									</div>
								</div>
							
							<div className="d-grid gap-2">
							<h5>Gallery:</h5>
							<ReactPlayer width="100%" url={video} />
      						<img className="d-block w-100 py-2" 
								src={imageMain}
	      						border="0" alt={name}/>
	      					      				
      						<img 
							className="d-block w-100 py-2" 
							src={image002}
      						border="0" alt={name}/>
      						<img 
							className="d-block w-100 py-2" 
							src={image003}
      						border="0" alt={name}/>
      						<img 
							className="d-block w-100 py-2" 
							src={image004}
      						border="0" alt={name}/>
							</div>
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}