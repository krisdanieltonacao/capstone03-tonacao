
import AdCompleteOrders from "../components/AdCompleteOrders";
import AdPendingOrders from "../components/AdPendingOrders";
import AdPaidOrders from "../components/AdPaidOrders";
import AdShippedOrders from "../components/AdShippedOrders";

import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import {Button} from "react-bootstrap";
import {Link} from "react-router-dom";


export default function ShowOrders() {
  return (
	<>
	 <div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				<Button as={Link} to="/addProduct" variant="primary" size="lg" className="mx-2">Add Product</Button>
				<Button as={Link} to="/admin" variant="success" size="lg" className="mx-2">Show Products</Button>
				<Button as={Link} to="/allOrders" variant="grey" size="lg" className="mx-2">Show Orders</Button>
        <Button as={Link} to="/archived" variant="secondary" size="lg" className="mx-2">Archived Orders</Button>
			</div>
			
	
	<h6 className="text-center">Click on a tab to check desired status</h6>
    <Tabs
      defaultActiveKey="Pending"
      id="justify-tab-example"
      className="mb-3"
      justify
    >
      <Tab eventKey="Pending" title="PENDING">
	  <AdPendingOrders />
      </Tab>
      <Tab eventKey="Paid" title="PAID">
	  <AdPaidOrders />
      </Tab>
      <Tab eventKey="Shipped" title="SHIPPED">
	  <AdShippedOrders />
      </Tab>
      <Tab eventKey="Completed" title="COMPLETE">
	  <AdCompleteOrders />
      </Tab>
     
    </Tabs>
	</>
  );
}

