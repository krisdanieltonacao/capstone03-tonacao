import {useState, useEffect} from "react";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import {UserProvider} from "./UserContext";
import AppNavbar from "./components/AppNavbar";

import AdminDashboard from "./pages/AdminDashboard";
import AdArchived from "./pages/AdArchived";
import AddProduct from "./pages/AddProduct";
import EditProduct from "./pages/EditProduct";
import EditUserDetails from "./pages/EditUserDetails";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import NSwitch from "./pages/NSwitch";
import PS4 from "./pages/PS4";
import PS5 from "./pages/PS5";
import Xbox from "./pages/Xbox";
import Apple from "./pages/Apple";
import Samsung from "./pages/Samsung";
import OneProductView from "./pages/OneProductView";
import Products from "./pages/Products";
import Register from "./pages/Register";
import CartPage from "./pages/CartPage";
import ShowOrders from "./pages/ShowOrders";

import {Container} from "react-bootstrap";
import './App.css';

function App() {
 const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })
   const unsetUser = () =>{
    localStorage.clear();
  }

  useEffect(() =>{
    
  }, [user])

  useEffect(() =>{
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
        if(typeof data._id !== "undefined"){
          setUser({
              id: data._id,
            isAdmin: data.isAdmin
          })
      }
       else{
          setUser({
              id: null,
              isAdmin: null
            })
      }
      
    })
  }, [])

 return(
       <UserProvider value={{user, setUser, unsetUser}}>
         <Router>
          <AppNavbar />
          <Container fluid>
               <Routes>
                  <Route exact path ="/" element={<Home />} />
                  <Route exact path ="/admin" element={<AdminDashboard />} />
                  <Route exact path="/addProduct" element={<AddProduct />}/>
                  <Route exact path="/editProduct/:productId" element={<EditProduct />}/>
                  <Route exact path ="/login" element={<Login />} />
                  <Route exact path ="/logout" element={<Logout />} />
                  <Route exact path ="/products" element={<Products />} />
                  <Route exact path ="/register" element={<Register />} />
                  <Route exact path ="/all/NSwitch" element={<NSwitch />} />
                  <Route exact path ="/all/PS4" element={<PS4 />} />
                  <Route exact path ="/all/PS5" element={<PS5 />} />
                  <Route exact path ="/all/XBOX" element={<Xbox />} />
                  <Route exact path ="/all/APPLE" element={<Apple />} />
                  <Route exact path ="/all/Samsung" element={<Samsung />} />
                  <Route exact path ="/cart" element={<CartPage />} />
                  <Route exact path ="/allOrders" element={<ShowOrders />} />
                  <Route exact path ="/archived" element={<AdArchived />} />
                  <Route exact path ="/edit" element={<EditUserDetails />} />
                  <Route exact path ="/products/:productId" element={<OneProductView />} />


{/*                  <Route exact path ="/courses" element={<Courses />} />
                  <Route exact path ="/courses/:courseId" element={<CourseView />} />
                  
                  
                  <Route exact path ="/logout" element={<Logout />} />
                  <Route exact path ="*" element={<Error />} />
*/}              
               </Routes>
          </Container>
        </Router>
          </UserProvider>
  )
}
export default App;
