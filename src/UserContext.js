// React Context
// it allows us to pass down and use(consume) data in any component we need in our React app without using props.
//It allows us to share data (state) accros components more easily.

// 3 Simple steps in using React Context
	// 1. Creating the context
	// 2. Providing the context
	// 3. Consuming the context

import React from "react";

// Creating the Context Object
// A context object as the name states is a data type of an object that can be used to store information that can be shared to other components within the React app.
const UserContext = React.createContext();

// The "Provider" component allows other components to consume/use the context object and supply the necessary information needed to the context object
// Any component which is not wrapped by our Provider will not have access to the value provided in the context.
export const UserProvider = UserContext.Provider;

export default UserContext;